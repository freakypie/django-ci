from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic.base import RedirectView
from django.contrib import admin


admin.autodiscover()


urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url="/ci-projects/")),
    url(r'^', include("ci.urls")),
    url(r'^accounts/', include("django.contrib.auth.urls", "auth", "auth")),
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT,)
