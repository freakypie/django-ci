from django import template
from django.core.exceptions import ImproperlyConfigured
from django.db.models import get_app
from django.template.loader_tags import ConstantIncludeNode, IncludeNode
from django.conf import settings
register = template.Library()

def do_include_ifapp(parser, token):
    """
    Loads a template and renders it with the current context if the specified
    application is in settings.INSTALLED_APPS.

    Example::

        {% includeifapp "shop" "foo/some_include" %}
    """
    bits = token.split_contents()
    if len(bits) != 3:
        raise TemplateSyntaxError, "%r tag takes two argument: the application label and the name of the template to be included" % bits[0]

    app_name, path = bits[1:]
    app_name = app_name.strip('"\'')

    if app_name not in settings.INSTALLED_APPS: 
        # maybe they gave it a shorter name...
        try:
            models = get_app(app_name)
        except ImproperlyConfigured:
            return template.Node()

    if path[0] in ('"', "'") and path[-1] == path[0]:
        return ConstantIncludeNode(path[1:-1])
    return IncludeNode(path)
register.tag('includeifapp', do_include_ifapp)

class IfAppNode(template.Node):
    """
    Checks to see whether or not the specified app name is in
    settings.INSTALLED_APPS.  If it is, the inner block will be rendered.

    Example 1::

        {% ifapp blog %}
        We have the Blog installed.
        {% endifapp %}

    Example 2::

        {% ifapp blog %}
        We have the Blog installed.
        {% else %}
        The blog is not installed
        {% endifapp %}

    There ya have it.
    """
    def __init__(self, app_name, nodelist_true, nodelist_false):
        self.app_name = app_name
        self.nodelist_true, self.nodelist_false = nodelist_true, nodelist_false

        try:
            models = get_app(app_name)
            self.app_installed = True
        except ImproperlyConfigured:
            if app_name in settings.INSTALLED_APPS:
                self.app_installed = True
            else:
                self.app_installed = False

    def __repr__(self):
        return "<IfApp node>"

    def __iter__(self):
        if self.app_installed:
            for node in self.nodelist_true:
                yield node
        else:
            for node in self.nodelist_false:
                yield node

    def get_nodes_by_type(self, nodetype):
        nodes = []
        if isinstance(self, nodetype):
            nodes.append(self)

        if self.app_installed:
            nodes.extend(self.nodelist_true.get_nodes_by_type(nodetype))
        else:
            nodes.extend(self.nodelist_false.get_nodes_by_type(nodetype))

        return nodes

    def render(self, context):
        if self.app_installed:
            return self.nodelist_true.render(context)
        else:
            return self.nodelist_false.render(context)

@register.tag
def ifapp(parser, token):
    try:
        tag, app_name = token.contents.split()
    except ValueError:
        raise template.TemplateSyntaxError('ifapp syntax: {% ifapp misc %}')

    nodelist_true = parser.parse(('else', 'endifapp'))
    token = parser.next_token()
    if token.contents == 'else':
        nodelist_false = parser.parse(('endifapp',))
        parser.delete_first_token()
    else:
        nodelist_false = template.NodeList()

    return IfAppNode(app_name, nodelist_true, nodelist_false)
