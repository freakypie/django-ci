from django import template
from django.conf import settings
from django.db.models.fields.files import ImageFieldFile
from PIL import Image
import os
import logging


register = template.Library()


@register.filter
def thumbnail(url, args):
    """
    Provides a simple thumbnail filter.  Accepts one or two parameters:

    - width: the maximum width of the thumbnail
    - height: the maximum height of the thumbnail

    If one parameter is specified, the other image will be scaled to fit it.  If
    both are supplied, the lesser of the two will be used.
    """
    width = None
    height = None
    suffix = '_tn'
    default_ext = "png"
    args = args.split(',')
    for arg in args:
        k, v = arg.split('=')
        if k == 'width':
            width = int(v)
            suffix += '_w%i' % width
        elif k == 'height':
            height = int(v)
            suffix += '_h%i' % height
        else:
            raise template.TemplateSyntaxError('Invalid thumbnail parameter: "%s"' % k)

    # account for templates that are formatted the old way
    if isinstance(url, ImageFieldFile):
        url = url.url

    if url is None:
        return ""

    def trim_media(path, prefix):
        if path.startswith(prefix):
            return path[len(prefix):]
        else:
            return path
    # see if we have a thumbnail for this already
    rel_path = trim_media(url, settings.MEDIA_URL)
    full_path = os.path.join(settings.MEDIA_ROOT, rel_path)
    tn_full_path = os.path.join(settings.MEDIA_ROOT, "thumbnails", rel_path)
    fn, ext = os.path.splitext(tn_full_path)
    if not ext:
        ext = default_ext

    tn_file = fn + suffix + '.' + ext.strip(".")
    tn_rel_path = trim_media(tn_file, settings.MEDIA_ROOT).strip('/')
    tn_url = os.path.join(settings.MEDIA_URL, tn_rel_path)

    if os.access(tn_file, os.R_OK):
        # the thumbnail seems to exist... use it instead of making a new one
        return tn_url

    try:
        dirname = os.path.dirname(tn_full_path)
        os.makedirs(dirname)
    except (OSError, IOError):
        pass

    try:
        # try to open the image
        im = Image.open(full_path)
    except IOError, ex:
        # if we can't open it, just return the input URL
        return url
    else:
        # otherwise, attempt to make a thumbnail
        orig_w, orig_h = im.size

        # borrowed from Satchmo
        if orig_w is None or orig_h is None:
            # something is wrong with image
            return url

        # make proper size
        if width is not None and height is not None:
            if (orig_w == width) and (orig_h == height):
                # same dimensions
                return url
            size = (width, height)
        elif width is not None:
            if orig_w == width:
                # same dimensions
                return url
            size = (width, orig_h)
        elif height is not None:
            if orig_h == height:
                # same dimensions
                return url
            size = (orig_w, height)

        im.thumbnail(size, Image.ANTIALIAS)
        try:
            im.save(tn_file)
        except:
            # if you fail try try again
            try:
                im.save("%s.%s" % (tn_file, default_ext))
            except:
                logging.error("Error saving thumbnail", exc_info=True)

    return tn_url
