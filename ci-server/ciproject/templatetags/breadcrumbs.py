from django.contrib.flatpages.models import FlatPage
from django.utils.safestring import mark_safe
from django import template

register = template.Library()

@register.filter
def breadcrumbs(url):
    parts = url.strip("/").split("/")
    crumbs = ""
    url = "/"
    for part in parts[:-1]:
        if part:
            url += part + "/"
            try:
                fp = FlatPage.objects.get(url=url)
                crumbs += " <a href='%s'>%s</a> &rsaquo; " % (fp.url, fp.title)
            except FlatPage.DoesNotExist:
                pass

    return mark_safe(crumbs)
