from django import template
from django.core.serializers import serialize
from django.db import models
from django.db.models.query import QuerySet
from django.utils import simplejson
from django.template import Library
from django.forms.models import model_to_dict

register = Library()

@register.filter()
def json_backslash(text):
    """
    escapes chars for json strings
    """
    text ="%s" % text
    return text.replace('\\','\\\\').replace('"','\\"')
json_backslash.is_safe = True

@register.filter()
def jsonify(object):
    if isinstance(object, QuerySet):
        return serialize('json', object)
    if isinstance(object, models.Model):
        return simplejson.dumps(model_to_dict(object))
    return simplejson.dumps(object)