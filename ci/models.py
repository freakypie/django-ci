from ci.virtualenvapi.manage import VirtualEnvironment, execute
from django.core.mail import send_mail
from django.core.management import call_command
from django.db import models
from django.db.models import Q
from django.db.models.aggregates import Count
from django.template.defaultfilters import slugify
from os import path
from socketio_server.options import emit_to_all
from subprocess import Popen, STDOUT
import datetime
import gevent
import os
import re
import shlex
import shutil
import signal
import subprocess
import traceback
from fabric.context_managers import lcd, prefix, path as fabric_path
from fabric.operations import local
import platform
import logging
import json
import functools
from django.conf import settings


class Server:
    ports = range(8003, 8011)
    available_ports = ports[:]
    servers = {}


NOTIFICATIONS = (
    ("test_started", "A test suite has started"),
    ("test_ended", "A test suite has finished"),
    ("test_failures", "A test suite reported failures"),
    ("test_success", "A test suite reported 100% success"),
)

BUILD_DIR = getattr(settings, "BUILD_DIR", "/tmp/builds")


class NotificationPrefManager(models.Manager):
    pass


def fetch_attrib(name):
    def inner(self):
        return self.get_query_set().get(notification=name)
    return inner

for name, desc in NOTIFICATIONS:
    setattr(NotificationPrefManager, name, fetch_attrib(name))


class NotificationPref(models.Model):
    user = models.ForeignKey("auth.User", related_name="notifications")
    notification = models.CharField(max_length=30, choices=NOTIFICATIONS)
    email = models.BooleanField(blank=True)
    html = models.BooleanField(blank=True)

    objects = NotificationPrefManager()


class CiProject(models.Model):
    name = models.CharField(max_length=50)
    slug = models.CharField(max_length=50, blank=True)
    url = models.CharField(max_length=255)
    commands = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return u"%s" % (self.name)

    def save(self, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super(CiProject, self).save(**kwargs)


class CiBranch(models.Model):
    class Meta:
        ordering = ("created_on",)
        unique_together = ("project", "name")
        verbose_name_plural = "Ci-Branches"
    project = models.ForeignKey(CiProject, related_name="branches")
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50)
    commands = models.TextField(blank=True, null=True)
    output = models.TextField(null=True, editable=False)
    created_on = models.DateTimeField(auto_now_add=True)
    temp = models.BooleanField(default=False,
        help_text="if set, this branch will be erased automatically")

    def __unicode__(self):
        return u"%s@%s" % (self.project.name, self.name)

    def save(self, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super(CiBranch, self).save(**kwargs)

    @property
    def git_head(self):
        env = VirtualEnvironment(self.path)
        output = (env.execute(
            "git log --pretty=format:'" +
            "{" +
                "\"commit\": \"%H\"," +
                "\"author\": \"%an <%ae>\"," +
                "\"date\": \"%ad\"," +
                "\"message\": \"%s\"" +
            "}' -n 1"))
        print "-" * 20
        print output
        print
        try:
            output = json.loads(output)
        except:
            pass
        return output

    @property
    def path(self):
        name = "%s_%s" % (self.project.slug, self.slug[:8])
        path = '%s/%s/' % (BUILD_DIR, name)
        return path

    def setup_venv(self):

        env = VirtualEnvironment(self.path)

        print "checking out code"
        self.pull(env)

        return env

    def pull(self, env):
        print("before check", env.exists("."))
        if not env.exists("."):
            print(self.path)
            execute("git clone %s %s" % (self.project.url, self.path))
        print(env.path)

        git_url = env.execute("git config --get remote.origin.url").strip()
        if git_url != self.project.url.strip():
            raise Exception("Attempting to work with incorrect repo (%s != %s)" % (
                git_url, self.project.url))

        if len(self.name) < 40:  # refspec or branch?
            env.execute("git pull --no-edit")
        else:
            env.execute("git fetch")

        env.execute("git checkout %s" % self.name)

    def _test_update(self, message):
	build_msg = getattr(self, "build_msg", None)
	if build_msg:
	    logging.warn("informing gitlabci of log")
            response2 = build_msg(dict(
                state="running",
                trace=message
            ))

        logging.warning("%s-%s: %s", self.project.name, self.name, message)
        packet = dict(
            name=self.project.name,
            branch=self.name,
            message=message
        )
        emit_to_all("test_progress", packet)

    def test(self):
        success = True
        log = u""
        try:
            self._test_update("Test Started")

            self._test_update("Clearing previous Test Results")
            self.testrun_set.all().delete()

            self._test_update("Setting Up Virtual Environment")
            env = self.setup_venv()

            commands = self.commands or self.project.commands
            for line in commands.split("\n"):
                if line.strip():
                    log += u"Running: `%s`\n" % line
                    self._test_update("Running: `%s`" % line)
                    process = env.execute(line,
                        return_process=True)
                    out, err = process.communicate()
                    log += out + u"-" * 20 + u"\n" + err
                    if process.returncode != 0:
                        print "TESTS FALED!"
                        success = False
                        break

            self._test_update("Done")
        except Exception, ex:
            traceback.print_exc()

            self._test_update("Exception! %s" % ex)
            TestRun.objects.create(
                branch=self,
                module=TestModule.objects.get_or_create(name="ci")[0],
                name="Setting up Test",
                status="fail",
                description=ex,
                error=traceback.format_exc()
            )
            log += traceback.format_exc() + u".\n Couldn't even test, setup totally bombed."
            success = False

        self.output = log
        self._test_update("Saving test results")
        self.save()

        self._test_update("Parsing output")
        self.parse_output()

        return success, log
#
#         if self.stats().filter(status="fail").count() > 0:
#             emails = NotificationPref.objects.filter(
#                 notification="test_failures",
#                 email=True
#             ).values_list("user__email", flat=True)
#
#             if emails:
#                 send_mail("Test Failures", "%s %s" % (self.project.name, self.name),
#                     from_email="ci-server@leithall.com",
#                     recipient_list=emails)
#         else:
#             emails = NotificationPref.objects.filter(
#                 notification="test_success",
#                 email=True
#             ).values_list("user__email", flat=True)
#
#             if emails:
#                 send_mail("Test Success", "%s %s" % (self.project.name, self.name),
#                     from_email="ci-server@leithall.com",
#                     recipient_list=emails)

    def parse_output(self, output=None):
        if not output:
            output = self.output

        results = []

        idx = 0
        try:

            self.testrun_set.all().delete()

            while idx < output:
                idx = output.index("=" * 70, idx)
                if idx == -1:
                    break

                header_end = output.index("-" * 70, idx + 71)
                stack_end = output.index("-" * 70, header_end + 71)

                header = output[idx + 71:header_end]
                header, description = header.split("\n", 1)
                tag, name = header.split(":", 1)
                name, module = name.split("(", 1)
                module = module.strip("()")
                stack = output[header_end + 71:stack_end]
                results.append(TestRun.objects.create(
                    branch=self,
                    name=name,
                    module=TestModule.objects.get_or_create(name=module)[0],
                    description=description,
                    status=tag.lower(),
                    error=stack,
                ))

                idx = stack_end + 71
        except ValueError:
            pass

        return results

    def stats(self):
        return self.testrun_set.values("status").annotate(count=Count("status"))

    def _create_server(self):

        if len(groups) and len(Server.available_ports) > 0:
            group = groups[0]
            env = self.setup_venv()

            port = Server.available_ports[0]
            Server.available_ports = Server.available_ports[1:]
            server = None
            for command in group.command_set.all():
                for line in command.commands.split("\n"):
                    cmds = shlex.split(line)
                    for i, cmd in enumerate(cmds):
                        if cmd == ":interface:":
                            cmds[i] = "0.0.0.0:%s" % port
                    if cmds:
                        server = env.execute(cmds, env=command.env,
                            return_process=True)

            if server:
                Server.servers[self] = {"port": port, "process": server}
            else:
                print "failed to start server"
        return self.get_server()

    def _stop_server(self):
        server = Server.servers.get(self)
        if server:

            server['process'].send_signal(signal.SIGINT)
            os.killpg(server['process'].pid, signal.SIGKILL)

            Server.available_ports.append(server['port'])
            del Server.servers[self]

    def get_server(self):
        if self in Server.servers:
            server = Server.servers.get(self)
            if not server['process'].poll() is None:
                Server.available_ports.append(server['port'])
                del Server.servers[self]

        return Server.servers.get(self)

    def get_server_url(self, request):
        server = self.server()
        if server:
            return "http://%s:%s" % (request.get_host(), server[0])

    @property
    def last_run(self):
        return self.testrun_set.latest("created_at")

    def delete(self, using=None):
	try:
            shutil.rmtree(self.path)
        except OSError:
            # The directory has been deleted or was never created
            pass
        models.Model.delete(self, using=using)


class TestModule(models.Model):
    class Meta:
        ordering = ("name",)
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class TestRunManager(models.Manager):

    def failures(self):
        return self.get_query_set().filter(status__in=("fail", "error", "unexpected success"))

    def successes(self):
        return self.get_query_set().exclude(status__in=("fail", "error", "unexpected success"))


class TestRun(models.Model):
    class Meta:
        ordering = ("module", "name",)
    created_at = models.DateTimeField(auto_now=True)
    branch = models.ForeignKey(CiBranch)
    module = models.ForeignKey(TestModule)
    name = models.CharField(max_length=255)
    status = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    error = models.TextField(blank=True, null=True)

    objects = TestRunManager()

    def __unicode__(self):
        return self.name

    def label(self):
        return {
            "success": "success",
            "skip": "default",
            "fail": "important",
            "error": "important",
            "expected failure": "success",
            "unexpected success": "important",
        }.get(self.status, "important")
