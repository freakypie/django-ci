
from ConfigParser import RawConfigParser
from ci.models import CiProject, CiBranch, TestModule, TestRun, NotificationPref
from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponse
from django.shortcuts import redirect, get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View, TemplateView
from django.views.generic.detail import DetailView, SingleObjectMixin
from django.views.generic.edit import UpdateView, FormView
from pprint import pprint
from subprocess import Popen
from viewsets import ViewSet, ViewSetMixin
import datetime
import gevent
import json
import os
import requests
import subprocess
import traceback
import urllib2
import logging
import functools


class BaseViewSet(ViewSet):

    def protect(self, function):
        if not hasattr(function, "protected"):
            def inner(self, request, *args, **kwargs):
#                 if not request.user.is_authenticated():
#                     return redirect("%s?next=%s" % (settings.LOGIN_URL, request.path))
                return function(self, request, *args, **kwargs)
            inner.protected = True
            return inner
        return function

    def get_success_url(self):
        return reverse(self.manager.default_app + ":detail",
            args=[self.object.slug],
            current_app=self.manager.name)


class CiProjectViewSet(BaseViewSet):
    object_url = "(?P<slug>[\w\d\-]+)/"

    def get_success_url(self):
        return reverse(self.manager.default_app + ":detail",
            args=[self.project.slug, self.object.slug],
            current_app=self.manager.name)


class CiBranchViewSet(BaseViewSet):
    base_url = "^ci-projects/(?P<project>[\w\-]+)/branch/"
    object_url = "(?P<slug>[\w\-\d]+)/"

    def get_queryset(self, view, request, **kwargs):
        view.project = get_object_or_404(CiProject, slug=kwargs.get("project"))
        retval = CiBranch.objects.filter(project=view.project)
        return retval


class NotificationViewSet(BaseViewSet):

    def get_queryset(self, view, request, **kwargs):
        if request.user.is_authenticated():
            return NotificationPref.objects.filter(user=request.user)
        else:
            return NotificationPref.objects.none()


ciprojects = CiProjectViewSet(model=CiProject, template_dir="ci/ci-projects")
cibranches = CiBranchViewSet(model=CiBranch, template_dir="ci/ci-branches")
notifications = NotificationViewSet(model=NotificationPref, template_dir="ci/notifications")


class GitLabCiConnectMixin(object):

    def dispatch(self, request, *args, **kwargs):
        if not hasattr(settings, "GITLAB_CI_URL"):
            messages.error(request, "You need to configure 'GITLAB_CI_URL' in your settings.py file")
            return redirect(request.META.get("HTTP_REFERER", "/"))

        return super(GitLabCiConnectMixin, self).dispatch(request, *args, **kwargs)

    def get_token(self):
        try:
            config = RawConfigParser()
            config.read("site.ini")
            return config.get("gitlab", "token")
        except:
            return None

    def save_token(self, data):
        config = RawConfigParser()
        config.read("site.ini")
        if not config.has_section("gitlab"):
            config.add_section("gitlab")
        config.set("gitlab", "id", data["id"])
        config.set("gitlab", "token", data["token"])

        with open('site.ini', 'wb') as configfile:
            config.write(configfile)

    def api_url(self, url):
        return settings.GITLAB_CI_URL.lstrip("/") + url

    def api_request(self, method, url, data={}):
        if "token" not in data:
            data.update(
                token=self.get_token()
            )

        response = requests.request(method, self.api_url(url),
            data=json.dumps(data),
            headers={"content-type": "application/json"})

        try:
            return response.json()
        except:
            print method, self.api_url(url), data
            print response.text
            print response.status_code

            raise Exception("FAIL!")


@ciprojects.register("gitlab-ci-connect", ordering=0)
class GitLabCiConnectView(ViewSetMixin, GitLabCiConnectMixin, FormView):

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        if self.get_token():
            messages.info(request, "Key already acquired (%s)" % self.get_token())
            return redirect(request.META.get("HTTP_REFERER", reverse_lazy("ci-projects:list")))

        return super(GitLabCiConnectView, self).dispatch(request, *args, **kwargs)

    def get_form_class(self):

        class ConnectForm(forms.Form):
            token = forms.CharField(help_text="Get this from the runners page of gitlab-ci")

        return ConnectForm

    def form_valid(self, form):

        if not os.path.exists("key.pub"):
            process = Popen(["ssh-keygen", "-f", "key", "-N", "", "-t", "rsa", "-C", "ci-server"])
            process.communicate()

        with open("key.pub") as fileh:
            key = fileh.read()

        response = self.api_request("post", "/runners/register.json", dict(
            public_key=key,
            token=form.cleaned_data['token']
        ))

        self.save_token(response)

        return redirect("ci-projects:list")


@ciprojects.register("gitlab-ci-builds", ordering=0)
class GitLabCiBuildsView(ViewSetMixin, GitLabCiConnectMixin, View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(GitLabCiBuildsView, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        gevent.spawn(self.get_job)
        messages.info(request, "Checking for new builds...")

        return redirect("ci-projects:list")

    def post(self, request):
        try:
            gevent.spawn_later(15, self.get_job)
            messages.info(request, "Checking for new builds...")
        except:
            traceback.print_exc()

        return redirect("ci-projects:list")

    def get_job(self):
        response = self.api_request("post", "/builds/register")
        # response = {u'commands': u'''fab build''',
        #    u'sha': u'6fef1844de428846b27c9e8913f160617cc2523c',
        #    u'repo_url': u'ssh://git@git.velocitywebworks.com:4000/jleith/superior-threads.git',
        #    u'project_id': 3, u'ref': u'beta', u'id': 82}
        print response
        if response.get("id"):
            logging.info("Got job", response)
            gevent.spawn(self.run_job, response)
        else:
            logging.info("No new jobs", response)

    def run_job(self, response):

        # get project
        project = CiProject.objects.get_or_create(
            url=response['repo_url'],
            defaults=dict(
                name=os.path.basename(response['repo_url']).split(".", 1)[0]
            )
        )[0]

        # create branch
        branch = project.branches.get_or_create(
            name=response['sha'],
            defaults=dict(
                temp=True,
                commands=response['commands']
            )
        )[0]
        if branch.temp:
            branch.commands = response['commands']
            branch.save()

        # test
        try:
	    branch.build_msg = functools.partial(
		self.api_request, "put", "/builds/%s.json" % response['id'])
            success, results = branch.test()
            logging.warn("test results: %s %s" % (success, results))
        except:
            traceback.print_exc()
            logging.warn("Test threw exception!")
            success = False
            results = traceback.format_exc()

        state = success and "success" or "failed"

        # report
        logging.warn("informing gitlabci of status: %s - %s" % (success, state))
        response2 = self.api_request("put", "/builds/%s.json" % response['id'], dict(
            state=state,
            trace=results
        ))
        logging.warn("response to posting job results: %s" % response2)

        # cleanup
        logging.warn("cleaning up")
        gevent.spawn(self.delete_old_branches, project)
        logging.warn("done cleaning")

        # check for another
        gevent.spawn(self.get_job)

    def delete_old_branches(self, project):

        # delete old branches
        for branch in project.branches.filter(temp=True,
            created_on__lt=datetime.datetime.now() - datetime.timedelta(days=3)):

            branch.delete()


@ciprojects.instance_view("hook", ordering=3)
class HookDetailView(ViewSetMixin, DetailView):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return DetailView.dispatch(self, request, *args, **kwargs)

    def post(self, request, slug):
        try:
            package = json.loads(request.body)
            name = package['repository']['name']
            branch = package['ref'].rsplit("/")[-1]
            if name and branch:
                project = CiProject.objects.get_or_create(
                    name=name,
                    url=package['repository']['url']
                )[0]

                # check if branch was deleted
                after = package.get("after")
                commits = package.get("total_commits_count")

                if commits == 0 and len(after.replace("0", "")) == 0:

                    # branch was deleted
                    try:
                        branch = CiBranch.objects.get(
                            project=project,
                            name=branch
                        )
                        branch.delete()
                    except ObjectDoesNotExist:
                        pass
                else:
                    branch = CiBranch.objects.get_or_create(
                        project=project,
                        name=branch
                    )[0]

                    gevent.spawn_later(2, branch.test)
        except:
            traceback.print_exc()

        return HttpResponse("OK")


@cibranches.instance_view("test")
class TestDetailView(ViewSetMixin, DetailView):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return DetailView.dispatch(self, request, *args, **kwargs)

    def post(self, request, project, slug):
        self.object = self.get_object()

        gevent.spawn_later(2, self.object.test)

        messages.info(request, "test queued")

        return redirect(request.META.get("HTTP_REFERER",
            reverse_lazy("ci-projects:detail", args=[self.object.pk])))


@cibranches.instance_view("pull")
class PullProjectView(ViewSetMixin, SingleObjectMixin, View):

    def post(self, request, project, slug):
        self.object = self.get_object()
        gevent.spawn_later(2, self.object.setup_venv)
        messages.success(request, "Queued Pull")
        return redirect(request.META.get("HTTP_REFERER",
            reverse_lazy("ci-projects:detail", args=[self.object.pk])))


@cibranches.instance_view("start")
class StartServerView(ViewSetMixin, SingleObjectMixin, View):

    def post(self, request, project, slug):
        self.object = self.get_object()
        server = self.object.get_server()
        if not server:
            server = self.object._create_server()
            if not server:
                messages.error(request, "Sorry, couldn't create server")
        return redirect(request.META.get("HTTP_REFERER",
            reverse_lazy("ci-projects:detail", args=[self.object.pk])))


@cibranches.instance_view("stop")
class StopServerView(ViewSetMixin, SingleObjectMixin, View):

    def post(self, request, project, slug):
        self.object = self.get_object()
        self.object._stop_server()
        messages.success(request, "Server stopped")
        return redirect(request.META.get("HTTP_REFERER",
            reverse_lazy("ci-projects:detail", args=[self.object.pk])))
