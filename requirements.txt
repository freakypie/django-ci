requests
django>=1.4,<1.5
django_test_exclude
#python-daemon
django-viewsets
cython
git+https://github.com/surfly/gevent.git#egg=gevent
git+https://github.com/abourget/gevent-socketio.git#egg=gevent_socketio
git+https://freakypie@bitbucket.org/freakypie/django-socketio-server.git#egg=socketio_server
south